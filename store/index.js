export const state = () => ({
  counter: 0,
  darkMode: false
});
  
export const mutations = {
  increment (state) {
    state.counter++
  },
  TOGGLE_THEME: state => {
    state.darkMode = !state.darkMode
  }
};

export  const  getters  = {
  themeStatus: state => {
    return state.darkMode
  }
};